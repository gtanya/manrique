package edu.uoc.locuocomotive.controller;

import edu.uoc.locuocomotive.model.*;

import java.io.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class LocUOComotiveController {

    private List<Station> stations;
    private List<Route> routes;
    private List<Train> trains;
    private List<Passenger> passengers;
    private List<Ticket> tickets;
    private int currentSation;

    public LocUOComotiveController(String stationsFile, String routesFile, String trainsFile) {
        stations = new ArrayList<>();
        routes = new ArrayList<>();
        trains = new ArrayList<>();
        passengers = new ArrayList<>();
        tickets = new ArrayList<>();
        currentSation = 1;

        loadStations(stationsFile);
        loadTrains(trainsFile);
        loadRoutes(routesFile);
    }

    private void loadStations(String stationsFile) {
        InputStream is = getClass().getResourceAsStream("/data/" + stationsFile);

        if (is == null) {
            throw new NullPointerException("Cannot find resource file " + stationsFile);
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("\\|");

                addStation(Integer.parseInt(parts[0]), parts[1], parts[2], Integer.parseInt(parts[3]), parts[4], parts[5], Integer.parseInt(parts[6]), Integer.parseInt(parts[7]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadRoutes(String routesFile) {
        InputStream is = getClass().getResourceAsStream("/data/" + routesFile);

        if (is == null) {
            throw new NullPointerException("Cannot find resource file " + routesFile);
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("=");
                String[] parts2 = parts[0].split("\\|");

                addRoute(parts2[0], Integer.parseInt(parts2[1]), parts[1].split("\\|"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadTrains(String trainsFile) {
        InputStream is = getClass().getResourceAsStream("/data/" + trainsFile);

        if (is == null) {
            throw new NullPointerException("Cannot find resource file " + trainsFile);
        }

        try (BufferedReader br = new BufferedReader(new InputStreamReader(is))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("\\|");

                int[] seatsPerCar = new int[parts.length - 2];

                for (int i = 2; i < parts.length; i++) {
                    seatsPerCar[i - 2] = Integer.parseInt(parts[i]);
                }

                addTrain(Integer.parseInt(parts[0]), parts[1], seatsPerCar);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addStation(int id, String name, String city, int openingYear, String type, String image, int positionX, int positionY) {
        stations.add(new Station(id, name, city, openingYear, type, image, positionX, positionY));
    }

    public void addRoute(String id, int trainId, String... stationsAndTimes) {
        Train train = null;
        for (Train t : trains) {
            if (t.getId() == trainId) {
                train = t;
                break;
            }
        }

        if (train == null) {
            throw new IllegalArgumentException("Train with id " + trainId + " not found");
        }

        Map<Station, List<LocalTime>> stationsTimesMap = new LinkedHashMap<>();
        for (String entry : stationsAndTimes) {
            String[] parts = entry.split("\\[");
            if (parts.length != 2) {
                throw new IllegalArgumentException("Invalid format for station and times: " + entry);
            }

            String stationIdStr = parts[0];
            String timesStr = parts[1].replace("]", ""); // Elimina el caràcter de tancament de claudàtors

            int stationId;
            try {
                stationId = Integer.parseInt(stationIdStr);
            } catch (NumberFormatException e) {
                throw new IllegalArgumentException("Invalid station ID format: " + stationIdStr);
            }

            String[] times = timesStr.split(",");

            Station station = null;
            for (Station s : stations) {
                if (s.getId() == stationId) {
                    station = s;
                    break;
                }
            }

            if (station == null) {
                throw new IllegalArgumentException("Station with ID " + stationId + " not found");
            }

            List<LocalTime> timesList = new ArrayList<>();
            for (String time : times) {
                timesList.add(LocalTime.parse(time));
            }

            stationsTimesMap.put(station, timesList);
        }

        Route route = new Route(id, train, stationsTimesMap);
        routes.add(route);
    }



    public void addTrain(int id, String model, int... cars) {
        List<Car> carList = new ArrayList<>();
        for (int i = 0; i < cars.length; i++) {
            carList.add(new Car("C" + (i + 1), getSeatTypeByIndex(cars[i]), cars[i]));
        }
        trains.add(new Train(id, model, carList));
    }

    private SeatType getSeatTypeByIndex(int index) {

        if( 0 < index && index < 20) return SeatType.FIRST_CLASS;
        if( 20 <= index && index < 50) return SeatType.SECOND_CLASS;
        if( 50 <= index) return SeatType.THIRD_CLASS;
        return SeatType.COFFEE;
//        switch (index) {
//            case 0:
//                return SeatType.FIRST_CLASS;
//            case 1:
//                return SeatType.SECOND_CLASS;
//            case 2:
//                return SeatType.THIRD_CLASS;
//            default:
//                return SeatType.THIRD_CLASS;
//        }
    }

    public List<String> getStationsInfo() {
        List<String> info = new ArrayList<>();
        for (Station station : stations) {
            info.add(station.toString());
        }
        return info;
    }

    public String[] getSeatTypes() {
        return new String[]{"FIRST_CLASS", "SECOND_CLASS", "THIRD_CLASS"};
    }

    public List<String> getRoutesByStation(int stationId) {
        List<String> result = new ArrayList<>();

        // Recorrer cada ruta
        for (Route route : routes) {
            Map<Station, List<LocalTime>> stationsAndTimes = route.getStationsAndTimes();
            List<Station> stationList = new ArrayList<>(stationsAndTimes.keySet());

            // Cercar l'estació donada dins la ruta
            for (int i = 0; i < stationList.size() - 1; i++) { // Adjusted loop to prevent out of bounds
                Station station = stationList.get(i);
                if (station.getId() == stationId) {
                    Station nextStation = stationList.get(i + 1);
                    List<LocalTime> departureTimes = stationsAndTimes.get(station);
                    List<LocalTime> arrivalTimes = stationsAndTimes.get(nextStation);

                    // Construir les informacions de les rutes
                    for (int j = 0; j < departureTimes.size(); j++) {
                        LocalTime departureTime = departureTimes.get(j);
                        LocalTime arrivalTime = arrivalTimes.get(j);

                        long hours = java.time.Duration.between(departureTime, arrivalTime).toHours();
                        double ticketCost = hours * 30; // 30€/hora

                        String routeInfo = departureTime + "-" + arrivalTime + "|" +
                                route.getId() + "|" +
                                (int) ticketCost + "|" +  // Ens assegurem que el cost sigui enter
                                station.getId() + "|" +
                                nextStation.getId() + "|" +
                                station.getName() + "|" +
                                nextStation.getName();

                        result.add(routeInfo);
                    }
                }
            }
        }

        // Ordenar les rutes per hora de sortida
        result.sort(Comparator.comparing(s -> LocalTime.parse(s.split("\\|")[0].split("-")[0])));

        return result;
    }

    private Station getStationById(int id) {
        for (Station station : stations) {
            if (station.getId() == id) {
                return station;
            }
        }
        return null;
    }


    public void addPassenger(String passport, String name, String surname, LocalDate birthdate, String email) throws Exception {
        if (passport == null || passport.isEmpty()) {
            throw new Exception("Invalid passport");
        }
        if (name == null || name.isEmpty()) {
            throw new Exception("Invalid name");
        }
        if (surname == null || surname.isEmpty()) {
            throw new Exception("Invalid surname");
        }
        if (birthdate == null) {
            throw new Exception("Invalid birthdate");
        }
        if (email != null && !email.isEmpty()) {
            if (!email.matches("^[\\w-.]+@[\\w-]+\\.[\\w-]{2,4}$")) {
                throw new Exception("Invalid email");
            }
        }

        // Comprova si el passatger ja existeix i actualitza les dades
        for (Passenger passenger : passengers) {
            if (passenger.getPassport().equals(passport)) {
                passenger.setName(name);
                passenger.setSurname(surname);
                passenger.setBirthdate(birthdate);
                passenger.setEmail(email);
                return;
            }
        }

        // Afegeix un nou passatger si no existeix
        passengers.add(new Passenger(passport, name, surname, birthdate, email));
    }



    public Ticket createTicket(String passengerId, String routeId, LocalTime departureTime, LocalTime arrivalTime, double cost, int originStationId, int destinationStationId, String seatType) {
        // Buscar passatger
        Passenger passenger = null;
        for (Passenger p : passengers) {
            if (p.getPassport().equals(passengerId)) {
                passenger = p;
                break;
            }
        }
        if (passenger == null) {
            throw new IllegalArgumentException("Passenger with id " + passengerId + " not found");
        }

        // Buscar ruta
        Route route = null;
        for (Route r : routes) {
            if (r.getId().equals(routeId)) {
                route = r;
                break;
            }
        }
        if (route == null) {
            throw new IllegalArgumentException("Route with id " + routeId + " not found");
        }

        // Convertir LocalTime a LocalDateTime
        LocalDateTime departureDateTime = LocalDateTime.of(LocalDate.now(), departureTime);
        LocalDateTime arrivalDateTime = LocalDateTime.of(LocalDate.now(), arrivalTime);

        // Convertir seatType a SeatType
        SeatType seatTypeEnum;
        try {
            seatTypeEnum = SeatType.valueOf(seatType.toUpperCase());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid seat type: " + seatType);
        }

        // Trobar seient disponible
        String availableSeat = findAvailableSeat(route, seatTypeEnum);

        // Crear bitllet
        Ticket ticket = new Ticket(passenger, route, departureDateTime, arrivalDateTime, cost, originStationId, destinationStationId, seatTypeEnum, availableSeat);
        tickets.add(ticket);

        // Marcar tots els seients com disponibles
        for (Car car : route.getTrain().getCars()) {
            for (Seat seat : car.getSeats()) {
                seat.setAvailable(true);
            }
        }

        return ticket;
    }


    private String findAvailableSeat(Route route, SeatType seatType) {
        for (Car car : route.getTrain().getCars()) {
            if (car.getSeatType().equals(seatType)) {
                for (Seat seat : car.getSeats()) {
                    if (seat.isAvailable()) {
                        seat.setAvailable(false); // Marca el seient com ocupat
                        return car.getId() + "-" + seat.getId();  // Combina els IDs correctament
                    }
                }
            }
        }
        throw new IllegalStateException("No available seat found for the specified seat type: " + seatType);
    }

    public String getPassengerInfo(String passport) {
        for (Passenger passenger : passengers) {
            if (passenger.getPassport().equals(passport)) {
                return passenger.toString();
            }
        }
        return null;
    }

    public String getTrainInfo(int trainId) {
        for (Train train : trains) {
            if (train.getId() == trainId) {
                return train.getId() + "|" + train.getModel() + "|" + train.getCars().size();
            }
        }
        return null;
    }

    public List<String> getPassengerTickets(String passport) {
        List<String> info = new ArrayList<>();
        for (Ticket ticket : tickets) {
            if (ticket.getPassenger().getPassport().equals(passport)) {
                info.add(ticket.toString());
            }
        }
        // Representa que hem validat una estació?
        currentSation++;
        return info;
    }

    public List<String> getRouteDeparturesInfo(String routeId) {
        for (Route route : routes) {
            if (route.getId().equals(routeId)) {
                List<String> departuresInfo = new ArrayList<>();
                Map<Station, List<LocalTime>> stationsAndTimes = route.getStationsAndTimes();
                for (Map.Entry<Station, List<LocalTime>> entry : stationsAndTimes.entrySet()) {
                    departuresInfo.add(entry.getKey().getId() + "|" + entry.getValue());
                }
                return departuresInfo;
            }
        }
        return Collections.emptyList();
    }


    public int getCurrentStationId() {
        return currentSation;
    }

    private boolean isValidEmail(String email) {
        String emailRegex = "^[A-Za-z0-9+_.-]+@(.+)$";
        return email.matches(emailRegex);
    }

    public void buyTicket(String passport, String name, String surname, LocalDate birthdate, String email,
                          String routeId, LocalTime departureTime, LocalTime arrivalTime, double cost,
                          int originStationId, int destinationStationId, String selectedSeatType) throws Exception {

        // Afegir el passatger si no existeix
        addPassenger(passport, name, surname, birthdate, email);

        // Buscar el passatger pel passaport
        Passenger passenger = null;
        for (Passenger p : passengers) {
            if (p.getPassport().equals(passport)) {
                passenger = p;
                break;
            }
        }
        if (passenger == null) {
            throw new IllegalArgumentException("Passenger with passport " + passport + " not found");
        }

        // Buscar la ruta pel routeId
        Route route = null;
        for (Route r : routes) {
            if (r.getId().equals(routeId)) {
                route = r;
                break;
            }
        }
        if (route == null) {
            throw new IllegalArgumentException("Route with id " + routeId + " not found");
        }

        // Crear el bitllet
        createTicket(passport, routeId, departureTime, arrivalTime, cost, originStationId, destinationStationId, selectedSeatType);

    }

    public List<String> getAllTickets() {
        List<String> info = new ArrayList<>();
        for (Ticket ticket : tickets) {
            info.add(ticket.toString());
        }
        return info;
    }

}

