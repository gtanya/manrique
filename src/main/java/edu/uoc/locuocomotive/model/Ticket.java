package edu.uoc.locuocomotive.model;

import java.time.LocalDateTime;

public class Ticket {
    private Passenger passenger;
    private Route route;
    private LocalDateTime departureTime;
    private LocalDateTime arrivalTime;
    private double cost;
    private int originStationId;
    private int destinationStationId;
    private SeatType seatType;
    private String seatId; // Afegit el camp seatId

    public Ticket(Passenger passenger, Route route, LocalDateTime departureTime, LocalDateTime arrivalTime,
                  double cost, int originStationId, int destinationStationId, SeatType seatType, String seatId) { // Afegit seatId al constructor
        this.passenger = passenger;
        this.route = route;
        this.departureTime = departureTime;
        this.arrivalTime = arrivalTime;
        this.cost = cost;
        this.originStationId = originStationId;
        this.destinationStationId = destinationStationId;
        this.seatType = seatType;
        this.seatId = seatId; // Assigna el nou camp
    }

    // Getters i setters
    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public Route getRoute() {
        return route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public LocalDateTime getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(LocalDateTime departureTime) {
        this.departureTime = departureTime;
    }

    public LocalDateTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalDateTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getOriginStationId() {
        return originStationId;
    }

    public void setOriginStationId(int originStationId) {
        this.originStationId = originStationId;
    }

    public int getDestinationStationId() {
        return destinationStationId;
    }

    public void setDestinationStationId(int destinationStationId) {
        this.destinationStationId = destinationStationId;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public void setSeatType(SeatType seatType) {
        this.seatType = seatType;
    }

    public String getSeatId() {
        return seatId;
    }

    public void setSeatId(String seatId) {
        this.seatId = seatId;
    }

    @Override
    public String toString() {
        return route.getId() + "|" + departureTime.toLocalTime() + "|" + route.getStationById(originStationId).getName() + "|" +
                arrivalTime.toLocalTime() + "|" + route.getStationById(destinationStationId).getName() + "|" + seatId + "|" + cost;
    }

    private String getStationNameById(int stationId) {
        for (Station station : route.getStationsAndTimes().keySet()) {
            if (station.getId() == stationId) {
                return station.getName();
            }
        }
        return "Unknown";
    }
}
