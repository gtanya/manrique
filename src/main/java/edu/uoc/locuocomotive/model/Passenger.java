package edu.uoc.locuocomotive.model;

import java.time.LocalDate;

public class Passenger {
    private String passport;
    private String name;
    private String surname;
    private LocalDate birthdate;
    private String email;

    public Passenger(String passport, String name, String surname, LocalDate birthdate, String email) {
        this.passport = passport;
        this.name = name;
        this.surname = surname;
        this.birthdate = birthdate;
        this.email = email;
    }

    // Getters and setters
    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return passport + "|" + name + "|" + surname + "|" + birthdate + "|" + email;
    }
}
