package edu.uoc.locuocomotive.model;

import java.util.List;

public class Train {
    private int id;
    private String model;
    private List<Car> cars;

    public Train(int id, String model, List<Car> cars) {
        this.id = id;
        this.model = model;
        this.cars = cars;
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public String getModel() {
        return model;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    @Override
    public String toString() {
        return id + "|" + model + "|" + cars;
    }
}
