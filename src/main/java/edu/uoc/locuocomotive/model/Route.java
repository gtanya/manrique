package edu.uoc.locuocomotive.model;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;

public class Route {
    private String id;
    private Train train;
    private Map<Station, List<LocalTime>> stationsAndTimes;

    public Route(String id, Train train, Map<Station, List<LocalTime>> stationsAndTimes) {
        this.id = id;
        this.train = train;
        this.stationsAndTimes = stationsAndTimes;
    }

    public String getId() {
        return id;
    }

    public Train getTrain() {
        return train;
    }

    public Map<Station, List<LocalTime>> getStationsAndTimes() {
        return stationsAndTimes;
    }

    // Afegeix aquest mètode
    public Station getStationById(int stationId) {
        for (Station station : stationsAndTimes.keySet()) {
            if (station.getId() == stationId) {
                return station;
            }
        }
        throw new IllegalArgumentException("Station with id " + stationId + " not found");
    }
}
