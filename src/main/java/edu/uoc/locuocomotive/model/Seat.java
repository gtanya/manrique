package edu.uoc.locuocomotive.model;

public class Seat {
    private String id;
    private boolean available;

    public Seat(String id) {
        this.id = id;
        this.available = true;
    }

    public String getId() {
        return id;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    @Override
    public String toString() {
        return id + (available ? " (available)" : " (occupied)");
    }
}
