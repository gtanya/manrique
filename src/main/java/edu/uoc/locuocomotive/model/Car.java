package edu.uoc.locuocomotive.model;

import java.util.ArrayList;
import java.util.List;

public class Car {
    private String id;
    private SeatType seatType;
    private List<Seat> seats;

    public Car(String id, SeatType seatType, int numSeats) {
        this.id = id;  // Afegeix el prefix "C" als IDs dels cars
        this.seatType = seatType;
        this.seats = new ArrayList<>();
        for (int i = 1; i <= numSeats; i++) {
            seats.add(new Seat(String.valueOf(i)));    // Assigna els IDs correctes als seients
        }
    }

    public String getId() {
        return id;
    }

    public SeatType getSeatType() {
        return seatType;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    @Override
    public String toString() {
        return id + " (" + seatType + "): " + seats;
    }
}

